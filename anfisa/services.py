import requests


def what_weather(city):
    """
    This function send query to weather-site and get info about
    city, where friend live
    @params: city
    """
    url = f'http://wttr.in/{city}'
    weather_parameters = {
        'format': 2,
        'M': ''
    }
    try:
        response = requests.get(url, params=weather_parameters)
    except requests.ConnectionError:
        return '<Network Error>'
    if response.status_code == 200:
        return response.text.strip()
    else:
        return '<Error in weather server. Please, try again later>'


def what_temperature(weather):
    """
    This function takes weather from what_weather() function and
    parse the temperature from this.
    @params: weather
    """
    if (weather == '<Network Error>' or
        weather == '<Error in weather server. Please, try again later>'):
        return weather
    temperature = weather.split()[1]
    parsed_temperature = ''
    for char in temperature:
        if char == '-':
            parsed_temperature += char
        try:
            num = int(char)
            parsed_temperature += char
        except ValueError:
            continue
    return parsed_temperature

def what_conclusion(parsed_temperature):
    """
    This function takes temperature from what_temperature() and
    send some comment about this
    @params: parsed_temperature 
    """
    try:
        temperature = int(parsed_temperature)

        if temperature < 18:
            return 'Very Cold!!'
        elif temperature >= 18 and temperature <= 27:
            return 'This is good temperature!'
        else:
            return 'It is very hot!'

    except ValueError:
        return 'Cannot to find information about temperature...'

