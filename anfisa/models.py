# Dict friends_db contains friends and cities, where they live

friends_db = {
    'Dima': 'Astana',
    'Vasiliy': 'Ekaterinburg',
    'Stephan': 'Kalyazin',
    'Andrew': 'Ulyanovsk',
    'Artem': 'Moscow',
    'Anton': 'Orsk',
    'Nikolay': 'Krasnoyarsk',
    'Phill': 'Saint-Petersburg',
}
